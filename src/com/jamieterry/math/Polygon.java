package com.jamieterry.math;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class Polygon implements Serializable{
	private static final long serialVersionUID = -8881867063657697512L;
	
	private Vector2d[] mPoints;
	
	//creates new polygon from specified points
	public Polygon(float[] points){
		assert(points.length %2 == 0); //num floats must be even as each point contains 2 floats
		
		int numPoints = points.length/2;
		
		mPoints = new Vector2d[numPoints];
		for(int i = 0; i < numPoints; ++i){
			//construct vector at index i with floats at 2i and 2i+1
			//Vertex 0 -> index 0 and 1
			//Vertex 1 -> index 2 and 3
			//etc
			mPoints[i] = new Vector2d(points[(2*i)], points[(2*i)+1]);
		}
	}
	
	public Polygon(ArrayList<Vector2d> points){
		mPoints = new Vector2d[points.size()];
		for(int i = 0; i < points.size(); ++i){
			mPoints[i] = new Vector2d(points.get(i)); //copy point from array list into internal array
		}
	}
	//creates deep copy of existing polygon
	public Polygon(Polygon other){
		mPoints = new Vector2d[other.mPoints.length];
		for(int i = 0; i < mPoints.length; ++i){
			mPoints[i] = new Vector2d(other.mPoints[i]);
		}
	}
	
	public float getBoundingRadiusSquared(){
		float boundingRadiusSquared = 0;
		for(int i = 0; i < mPoints.length; ++i){
			float distance = mPoints[i].getLengthSquared();
			if(distance > boundingRadiusSquared){
				boundingRadiusSquared = distance;
			}
		}
		return boundingRadiusSquared;
	}
	
	public int getPointCount(){
		return mPoints.length;
	}
	
	public Vector2d getPointAtIndex(int index){
		return this.mPoints[index];
	}
	
	//returns true if this polygon contains the passed in point
	public boolean contains(Vector2d testPoint){
		//from here: http://stackoverflow.com/questions/8721406/how-to-determine-if-a-point-is-inside-a-2d-convex-polygon
		int i;
	    int j;
	    boolean result = false;
	    for (i = 0, j = mPoints.length - 1; i < mPoints.length; j = i++){
	      if ((mPoints[i].getY() > testPoint.getY()) != (mPoints[j].getY() > testPoint.getY()) &&
	          (testPoint.getX() < (mPoints[j].getX() - mPoints[i].getX()) * (testPoint.getY() - mPoints[i].getY()) / (mPoints[j].getY()-mPoints[i].getY()) + mPoints[i].getX())){
	        result = !result;
	       }
	    }
	    return result;
	}
	
	//returns true if this polygon completely contains the passed in polygon
	public boolean contains(Polygon otherPoly){
		for(Vector2d point : otherPoly.mPoints){
			if(!this.contains(point)){
				return false;
			}
		}
		
		//if still running then all other polys points are in this poly
		return true;
	}
	//returns true if this polygon intersects the other polygon
	public boolean intersects(Polygon other){
		
		LineSegment[] myLines = new LineSegment[this.mPoints.length];
		for(int i = 0; i < this.mPoints.length; ++i){
			myLines[i] = LineSegment.createFromStartEnd(this.mPoints[i], this.mPoints[(i+1)%this.mPoints.length]);
		}
		
		LineSegment[] theirLines = new LineSegment[other.mPoints.length];
		for(int j = 0; j < other.mPoints.length; ++j){
			theirLines[j] = LineSegment.createFromStartEnd(other.mPoints[j], other.mPoints[(j+1)%other.mPoints.length]);
		}
		
		for(int i = 0; i < myLines.length; ++i){
			for(int j = 0; j < theirLines.length; ++j){
				if(theirLines[j].getIntersection(myLines[i]) != null){
					//then the polygons are colliding
					return true;
				}
			}
		}
		
		//if still running, then no intersection
		return false;
	}
	
	public Vector2d[] getPoints(){
		Vector2d[] result = new Vector2d[mPoints.length];
		for(int i = 0; i < mPoints.length; ++i){ //make deep copy as we dont want calling code to edit the points in the polygon
			result[i] = new Vector2d(mPoints[i]);
		}
		return result;
	}
	
	//calculate and returns the width of the polygon
	public float findWidth(){
		float minX = Float.MAX_VALUE;
		float maxX = -Float.MAX_VALUE;
		for(int i = 0; i < mPoints.length; ++i){
			if(mPoints[i].getX() < minX){
				minX = mPoints[i].getX();
			}
			if(mPoints[i].getX() > maxX){
				maxX = mPoints[i].getX();
			}
		}
		return (maxX - minX);
	}
	
	//calculates and returns the width of the polygon
	public float findHeight(){
		float minY = Float.MAX_VALUE;
		float maxY = -Float.MAX_VALUE;
		for(int i = 0; i < mPoints.length; ++i){
			if(mPoints[i].getY() < minY){
				minY = mPoints[i].getY();
			}
			if(mPoints[i].getY() > maxY){
				maxY = mPoints[i].getY();
			}
		}
		return (maxY - minY);
	}
	
	public void rotate(float angle){
		for(int i = 0; i < mPoints.length; ++i){
			mPoints[i] = mPoints[i].getRotated(angle);
		}
	}
	
	public Polygon getRotated(float angle){
		Polygon result = new Polygon(this);
		result.rotate(angle);
		return result;
	}
	
	public void scale(float scale){
		for(Vector2d p : this.mPoints){
			p.multiplyEquals(scale);
		}
	}
	
	public Polygon getScaled(float scale){
		Polygon result = new Polygon(this);//make deep copy
		result.scale(scale);//scale it
		return result;//return it
	}
	
	public void translate(Vector2d offset){
		for(int i = 0; i < mPoints.length; ++i){
			mPoints[i].addEquals(offset);
		}
	}
	
	public Polygon getTranslated(Vector2d  offset){
		Polygon result = new Polygon(this);
		result.translate(offset);
		return result;
	}
	
	
}
