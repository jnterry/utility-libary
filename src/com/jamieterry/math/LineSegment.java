package com.jamieterry.math;

public class LineSegment {
	Vector2d mStart;
	Vector2d mOffset;
	
	//use the "create" methods (allows you to choose if you want to use start and end or
	//start and offset)
	private LineSegment(Vector2d start, Vector2d offset){
		mStart = start;
		mOffset = offset;
	}
	
	public static LineSegment createFromStartEnd(Vector2d start, Vector2d end){
		return new LineSegment(start, Vector2d.subtract(end, start));
	}
	
	//returns the intersection between two line segments, or null if they do not intersect
	public Vector2d getIntersection(LineSegment other){
		//t = (q - p) � s / (r � s)
		//u = (q - p) � r / (r � s)
		//http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
		float urs = (Vector2d.subtract(other.mStart, this.mStart)).crossProduct(this.mOffset); //calc u*rs
		float trs = (Vector2d.subtract(other.mStart, this.mStart)).crossProduct(other.mOffset); //calc t*rs
		float rs = this.mOffset.crossProduct(other.mOffset); //calc rs
		
		if(rs != 0){
			//then safe to divide by rs
			float t = trs/rs;
			float u = urs/rs;
			if(0 <= t && t <= 1 && 0 <= u && u <= 1){
				//then lines intersect
				return Vector2d.add(this.mStart, Vector2d.multiply(this.mOffset, t));
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	//returns the minimum distance between a point and the line squared
	public float getMinimumDistanceToPointSquared(Vector2d point){
		/*(//http://forums.codeguru.com/showthread.php?194400-Distance-between-point-and-line-segment
		//A is mStart
		//B is endPoint
		//C is point
		
		Vector2d endPoint = Vector2d.add(this.mStart, this.mOffset);
		
		return (float) Math.pow(((endPoint.getY()-mStart.getY())*(point.getX()-mStart.getX())+(endPoint.getX()-mStart.getX())*(point.getY()-mStart.getY())),2)/this.mOffset.getLengthSquared();
		
		/*float length = this.mOffset.getLengthSquared();
		Vector2d endPoint = Vector2d.add(this.mStart, this.mOffset);
		if(length == 0){
			return this.mStart.getDistanceToSquared(point);
			
		}
		float t = Vector2d.subtract(point, this.mStart)
				.dotProduct(Vector2d.subtract(endPoint, this.mStart));
		if(t < 0){
			//then the point is nearest point the lines start
			return this.mStart.getDistanceToSquared(point);
		} else if (t > 1){
			//then the point is nearest the end point of the line
			return endPoint.getDistanceToSquared(point);
		} else {
			//then point is nearest some point on the line
			return point.getDistanceTo(new Vector2d(
					this.mStart.getX() + t * (endPoint.getX() - this.mStart.getX()),
					this.mStart.getY() + t * (endPoint.getY() - this.mStart.getY())));
		}*/
		//return 0;*/
		
		float length = this.mOffset.getLengthSquared();
		Vector2d endPoint = Vector2d.add(this.mStart, this.mOffset);
		float t = Vector2d.subtract(point, this.mStart)
				.dotProduct(Vector2d.subtract(endPoint, this.mStart));
		if(t < 0){
			//then the point is nearest point the lines start
			return this.mStart.getDistanceTo(point);
		} else if (t > 1){
			//then the point is nearest the end point of the line
			return endPoint.getDistanceTo(point);
		} else {
			//then point is nearest some point on the line
			return new Vector2d(
					this.mStart.getX() + t * (endPoint.getX() - this.mStart.getX()),
					this.mStart.getY() + t * (endPoint.getY() - this.mStart.getY())).getDistanceTo(point);
		}
		
	}
	
	//returns the point on the line that is nearest the specified other point
	public Vector2d getPointOnLineClosestToPoint(Vector2d point){
		float length = this.mOffset.getLengthSquared();
		Vector2d endPoint = Vector2d.add(this.mStart, this.mOffset);
		if(length == 0){
			return this.mStart;
			
		}
		float t = Vector2d.subtract(point, this.mStart)
				.dotProduct(Vector2d.subtract(endPoint, this.mStart));
		if(t < 0){
			//then the point is nearest point the lines start
			return new Vector2d(this.mStart);
		} else if (t > 1){
			//then the point is nearest the end point of the line
			return endPoint;
		} else {
			//then point is nearest some point on the line
			return new Vector2d(
					this.mStart.getX() + t * (endPoint.getX() - this.mStart.getX()),
					this.mStart.getY() + t * (endPoint.getY() - this.mStart.getY()));
		}
	}
	
	//returns the point at the specified distance from the start of the line
	//point may be beyond the end of the line
	//-ve distanced will return a point in the oposite direction to the line's direction
	public Vector2d getPointAtDistance(float distance){
		Vector2d result = new Vector2d(mOffset);
		result.setMagnitude(distance);
		return result.addEquals(this.mStart);
	}
}
