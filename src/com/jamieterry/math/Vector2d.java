package com.jamieterry.math;

import java.io.Serializable;

public class Vector2d implements Serializable{
	private static final long serialVersionUID = 3086078509875585341L;
	public float x, y;
	
	////////////////////////////////////////////////////////////////////////
	//Setters and getters
	public float getX(){return x;}
	public float getY(){return y;}
	public void set(float newX, float newY){x = newX; y = newY;}
	public void setX(float newX){x = newX;}
	public void setY(float newY){y = newY;}
	
	////////////////////////////////////////////////////////////////////////
	//Constructors
	public Vector2d(){
		x = 0;
		y = 0;
	}
	//copys values from other vector
	public Vector2d(Vector2d otherVector){
		this.x = otherVector.x;
		this.y = otherVector.y;
	}
	
	public Vector2d(float newx, float newy){
		this.x = newx;
		this.y = newy;
	}
	
	////////////////////////////////////////////////////////////////////////
	//Basic Vector operators
	public Vector2d addEquals(Vector2d otherVector){ //this += other
		this.x += otherVector.x;
		this.y += otherVector.y;
		return this;
	}
	
	public static Vector2d add(Vector2d vectorA, Vector2d vectorB){ // Vector = A + B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.addEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d subtractEquals(Vector2d otherVector){ //this -= other
		this.x -= otherVector.x;
		this.y -= otherVector.y;
		return this;
	}
	
	public static Vector2d subtract(Vector2d vectorA, Vector2d vectorB){ // Vector = A - B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.subtractEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d divideEquals(Vector2d otherVector){ //this /= other
		this.x /= otherVector.x;
		this.y /= otherVector.y;
		return this;
	}
	
	public static Vector2d divide(Vector2d vectorA, Vector2d vectorB){ // Vector = A / B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.divideEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d multiplyEquals(Vector2d otherVector){ //this *= other
		this.x *= otherVector.x;
		this.y *= otherVector.y;
		return this;
	}
	
	public static Vector2d multiply(Vector2d vectorA, Vector2d vectorB){ // Vector = A * B
		Vector2d result = new Vector2d(vectorA);//copy vector a data into result
		result.multiplyEquals(vectorB); //now add a and b, using += code
		return result; //return result
	}
	
	public Vector2d multiplyEquals(float magnitudeModifier){
		this.x *= magnitudeModifier;
		this.y *= magnitudeModifier;
		return this;
	}
	
	public static Vector2d multiply(Vector2d vec, float magnitudeModifier){ // Vector = A * B
		Vector2d result = new Vector2d(vec);//copy vector a data into result
		result.multiplyEquals(magnitudeModifier); //now add a and b, using += code
		return result; //return result
	}
	
	////////////////////////////////////////////////////////////////////////
	//Other vector operators
	public float getLength(){
		//get distance between this vector and (0,0)
		return this.getDistanceTo(new Vector2d(0,0));
	}
	
	public float getLengthSquared(){
		return this.getDistanceToSquared(new Vector2d(0,0));
	}
	
	public float getDistanceTo(Vector2d otherVector){
		return (float) Math.sqrt(getDistanceToSquared(otherVector));
	}
	
	public float getDistanceToSquared(Vector2d otherVector){
		//distance squared = (deltaX^2)+(deltaY^2)
		return ((this.x - otherVector.x)*(this.x - otherVector.x)) + ((this.y - otherVector.y)*(this.y - otherVector.y));
	}
	
	public float getAngleTo(Vector2d otherVector){
		float result = (float) Math.atan((otherVector.y - this.y)/( otherVector.x - this.x));
		if (( otherVector.x - this.x) < 0){
			result += Math.PI;
		}
		return result * (180/3.14159265f);
	}
	
	//reverses THIS vector, use getReversed() if you do not wish to alter this vector
	public Vector2d reverseDirection(){
		x = - x;
		y = - y;
		return this;
	}
	
	//returns new vector that is the reverse of this one
	public Vector2d getReversed(){
		Vector2d result = new Vector2d(this);
		result.reverseDirection();
		return result;
	}
	
	public static Vector2d createVectorFromAngleAndMagnitude(float angle, float magnitude){
		return new Vector2d((float)(magnitude*Math.cos(angle)), (float)(magnitude*Math.sin(angle)));
	}
	public void set(Vector2d otherVector) {
		this.x = otherVector.x;
		this.y = otherVector.y;
		
	}
	
	//sets the magnitude (length) of this vector to specified value without changing its direction
	public void setMagnitude(float magnitude){
		float length = this.getLength();
		if(length != 0){
			//dividing by length makes the vector have length of 1, then times by new magnitude 
			this.x /= length;
			this.y /= length;
			
			this.x *= magnitude;
			this.y *= magnitude;
		}
	}
	
	
	//returns this vector rotated by "rotation" degrees clockwise, does not modify this vector
	public Vector2d getRotated(float rotation){
		float cs = (float) Math.cos(Math.toRadians(rotation));
		float sn = (float) Math.sin(Math.toRadians(rotation));
		Vector2d result = new Vector2d(x*cs - y*sn, x*sn + y*cs);
		return result;
	}
	
	public float crossProduct(Vector2d other){
		return (this.x*other.y)-(this.y*other.x);
	}
	
	public float dotProduct(Vector2d other){
		return ((this.x*other.x) + (this.y*other.y));
	}
	
	
	
	
	
	
	
	
}
