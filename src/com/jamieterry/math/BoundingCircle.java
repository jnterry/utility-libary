package com.jamieterry.math;

import java.util.Collection;
import com.jamieterry.math.Vector2d;

public class BoundingCircle{
	Vector2d mCenter;
	double mRadiusSquared;
	double mRadius;
	
	public BoundingCircle(){
		mCenter = new Vector2d();
		mRadiusSquared = 0;
		mRadius = 0;
	}
	
	//construct bounding sphere from points
	BoundingCircle(Collection<Vector2d> points){
		/////////////////////////////////////////////////////////////////////////////
		//first find centre of all points, this is just the average of all the points
		long totalX = 0;
		long totalY = 0;
		for(Vector2d v : points){
			totalX += v.getX();
			totalY += v.getY();
		}
		//divide total by number of points to find average
		mCenter = new Vector2d(totalX/points.size(), totalY/points.size());
		
		/////////////////////////////////////////////////////////////////////////////
		//now find farthest out point
		mRadiusSquared = -1;
		for(Vector2d v : points){
			double distance = v.getDistanceToSquared(mCenter);
			if(distance > mRadiusSquared){
				mRadiusSquared = distance;
			}
		}
		mRadius = Math.sqrt(mRadiusSquared);
	}
	
	public BoundingCircle(BoundingCircle bcToCopy) {
		this.mCenter = new Vector2d(bcToCopy.mCenter);
		this.mRadiusSquared = bcToCopy.mRadiusSquared;
		this.mRadius = bcToCopy.mRadius;
		
	}

	public boolean isCollidingWith(BoundingCircle otherSphere){
		double centerDistanceSquared = mCenter.getDistanceToSquared(otherSphere.mCenter)/4;
		return (centerDistanceSquared < mRadiusSquared || centerDistanceSquared < otherSphere.mRadiusSquared);
	}
	
	public boolean isCollidingWith(Vector2d point){
		return (mCenter.getDistanceToSquared(point) < this.mRadiusSquared);
	}
	
	public void expand(double size){
		this.mRadius += size;
		this.mRadiusSquared = this.mRadius*this.mRadius;
	}
	
	public void transform(Vector2d vec){
		this.mCenter.addEquals(vec);
	}
}
