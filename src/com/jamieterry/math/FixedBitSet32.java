package com.jamieterry.math;

import java.io.Serializable;

public class FixedBitSet32{
	int mBits;
	
	public FixedBitSet32(){
		mBits = 0;
	}
	
	public void setBit(int i){
		if(i < 0 || i > 32){
			throw new IndexOutOfBoundsException("FixedBitset index " + i + " is out of bounds!");
		}
		mBits |= (1 << i);
	}
	
	public void clearBit(int i){
		//~(1 << i)
		//the 1 produces 000001
		//lets assume i is 3
		//<< produces 001000
		//the ~ makes it 110111
		//the and means all bits we be kept as they are, except index i which will be set to 0
		mBits &= ~(1 << i);
	}
	
	public boolean getBit(int i){
		if(i < 0 || i > 32){
			throw new IndexOutOfBoundsException("FixedBitset index " + i + " is out of bounds!");
		}
		return (mBits & (1 << i)) == (1 << i);
	}
	
	//returns true if this bitset contains another bit set (ie, all bits set in passed in bit set 
	//are also set in this bitset
	public boolean contains(FixedBitSet32 otherBitset){
		return (otherBitset.mBits & this.mBits) == otherBitset.mBits;
	}
	
	
}
