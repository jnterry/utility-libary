package com.jamieterry.math;

import java.awt.geom.Area;
import java.util.Collection;

import com.jamieterry.math.Vector2d;

public class CollisionData{
	
	//the aabb of the collision area
	AABB mAABB;
	
	//the bounding circle of  the collision area
	BoundingCircle mBoundingCircle;
	
	//the shape making up the collision data
	Area mArea;
	
	//the local origin of the collision data object
	Vector2d mOrigin;
	
	CollisionData(Collection<Vector2d> points){
		mAABB = new AABB(points);
		mBoundingCircle = new BoundingCircle(points);
	}
	
	CollisionData(CollisionData cInfoToCopy){
		mAABB = new AABB(cInfoToCopy.mAABB);
		mBoundingCircle = new BoundingCircle(cInfoToCopy.mBoundingCircle);
	}
	
	public void transform(Vector2d vec){
		mAABB.transform(vec);
		mBoundingCircle.transform(vec);
	}
	
	
	public boolean isCollidingWith(CollisionData otherInfo){
		return (mAABB.isCollidingWith(otherInfo.mAABB) && 
				mBoundingCircle.isCollidingWith(otherInfo.mBoundingCircle));
	}
	public boolean isCollidingWith(Vector2d point){
		return (this.mAABB.isCollidingWith(point) &&
				this.mBoundingCircle.isCollidingWith(point));
	}
		
	//returns the area where two collision data objects are intersecting
	public Area getIntersectArea(CollisionData other){
		Area result = (Area) mArea.clone();
		result.intersect(other.mArea);
		return result;
	}

}
