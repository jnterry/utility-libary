package com.jamieterry.game;

import com.badlogic.gdx.math.Matrix4;

public abstract class Screen {
	
	public abstract void update();
	public abstract void handleInput();
	public abstract void render(Matrix4 projMat);
	
	public abstract void onDestroy();

}
